/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.pimlada.oopfinalexam;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Pimlada
 */
public class WriteOrder {
    public static void main(String[] args) {
        ArrayList<Order> orders = new ArrayList();
        orders.add(new Order("Ing", 2, "At restaurant", "Salad", 2, "Cocoa", 2));
        orders.add(new Order("Aun", 4, "Take home", "Salad", 1, "Lemon Tea", 1));
        
        File file = null;
        FileOutputStream fos;
        ObjectOutputStream oos = null;
        try {
            file = new File("order.bin");
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(orders);
            oos.close();
            fos.close();
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(WriteOrder.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(WriteOrder.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
