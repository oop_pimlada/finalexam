/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.pimlada.oopfinalexam;

import java.io.Serializable;


/**
 *
 * @author Pimlada
 */
public class Order implements Serializable {
    private String name;
    private int tableNo;
    private String status;
    private String menuLst;
    private int menuNo;
    private String drinkLst;
    private int drinkNo;

    public Order(String name, int tableNo, String status, String menuLst, int menuNo, String drinkLst, int drinkNo) {
        this.name = name;
        this.tableNo = tableNo;
        this.status = status;
        this.menuLst = menuLst;
        this.menuNo = menuNo;
        this.drinkLst = drinkLst;
        this.drinkNo = drinkNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTableNo() {
        return tableNo;
    }

    public void setTableNo(int tableNo) {
        this.tableNo = tableNo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMenuLst() {
        return menuLst;
    }

    public void setMenuLst(String menuLst) {
        this.menuLst = menuLst;
    }

    public int getMenuNo() {
        return menuNo;
    }

    public void setMenuNo(int menuNo) {
        this.menuNo = menuNo;
    }

    public String getDrinkLst() {
        return drinkLst;
    }

    public void setDrinkLst(String drinkLst) {
        this.drinkLst = drinkLst;
    }

    public int getDrinkNo() {
        return drinkNo;
    }

    public void setDrinkNo(int drinkNo) {
        this.drinkNo = drinkNo;
    }

    @Override
    public String toString() {
        return "Order{" + "name=" + name + ", tableNo=" + tableNo + ", status=" + status + ", menuLst=" + menuLst + ", menuNo=" + menuNo + ", drinkLst=" + drinkLst + ", drinkNo=" + drinkNo + '}';
    }
    
}
